﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IGeneratorContract
{
    public interface IExcelModel
    {
        List<Type> ColumnType { get; set; }
        List<string> ColumnName { get; set; }
        Dictionary<int, List<string>> Rows { get; set; }
    }
}
