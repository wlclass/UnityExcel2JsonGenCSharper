﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IGeneratorContract;

namespace GeneratorJson
{
    public class Test :  IPluginContract
    {

        public bool Execute(string path, out string msg)
        {
            var a = ExcelInfo; 
            msg = "error Log" + a.ColumnName[0];
            return true;
        }

        public string PluginName
        {
            get
            {
                return "测试模块";
            }
        }

        public string WarningConfirmText
        {
            get
            {
                return "你确认已经加载过文件吗？";
            }
        }

        public IExcelModel ExcelInfo { get; set; }
    }
}
